package scenarios

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object PageViews {

  val trafalgarPages = csv("TrafalgarPages.csv")
  val openbetPages = csv("OpenbetPages.csv")

  val headers = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "Accept-Encoding" -> "gzip, deflate, br",
    "User-Agent" -> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36"
  )

  val headersWithRedirects = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "Accept-Encoding" -> "gzip, deflate, br",
    "User-Agent" -> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36",
    "X-WH-SPORTS-REDIR-ENABLE" -> "yes"
  )

  // ramp simulation

  val viewTrafalgarPage = scenario("View Trafalgar Page Simulation")
    .forever {
      exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(trafalgarPages.random.circular)
        .exec(
          http("View Trafalgar Page")
            .get("${Url}")
            .headers(headers)
            .check(status.is(200))
        )
      //        .pause(1,5)
    }

  val viewOpenbetPage = scenario("View Openbet Page Simulation")
    .forever {
      exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(openbetPages.random.circular)
        .exec(
          http("View Openbet Page")
            .get("${Url}")
            .headers(headers)
            .check(status.is(200))
        )
      //        .pause(1,5)
    }

  // constant simulation

  val viewTrafalgarPageConstatly = scenario("View trafalgar page")
    .exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(trafalgarPages.random.circular)
        .exec(
          http("View trafalgar page")
            .get("${Url}")
            .headers(headers)
            .check(status.is(200))
        )

  val viewOpenbetPageConstatly = scenario("View openbet page")
    .exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(openbetPages.random.circular)
        .exec(
          http("View openbet page")
            .get("${Url}")
            .headers(headers)
            .check(status.is(200))
        )

  val viewTrafalgarPageConstatlyWithRedirects = scenario("View trafalgar page (with redirects)")
    .exec(session => session.reset)
    .exec(flushHttpCache)
    .exec(flushCookieJar)
    .exec(flushSessionCookies)
    .feed(trafalgarPages.random.circular)
    .exec(
      http("View trafalgar page (with redirects)")
        .get("${Url}")
        .headers(headersWithRedirects)
        .check(status.is(200))
    )

  val viewOpenbetPageConstatlyWithRedirects = scenario("View openbet page (with redirects)")
    .exec(session => session.reset)
    .exec(flushHttpCache)
    .exec(flushCookieJar)
    .exec(flushSessionCookies)
    .feed(openbetPages.random.circular)
    .exec(
      http("View openbet page (with redirects)")
        .get("${Url}")
        .headers(headersWithRedirects)
        .check(status.is(200))
    )
}
