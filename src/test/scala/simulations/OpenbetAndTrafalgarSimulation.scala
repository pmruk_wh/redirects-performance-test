package simulations

import io.gatling.core.Predef._
import scala.concurrent.duration._
import scenarios.PageViews._

class OpenbetAndTrafalgarSimulation extends Simulation with SimulationSettings {

  setUp(
    viewOpenbetPage.inject(rampProfile(maxUsers / 2)),
    viewTrafalgarPage.inject(rampProfile(maxUsers / 2)),
  ).maxDuration((paceDurationMinutes * RAMPS + rampDurationMinutes * RAMPS) minutes)

}
