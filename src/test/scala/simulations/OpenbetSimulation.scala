package simulations

import io.gatling.core.Predef._
import scala.concurrent.duration._
import scenarios.PageViews._

class OpenbetSimulation extends Simulation with SimulationSettings {

  setUp(viewOpenbetPage.inject(rampProfile(maxUsers))).maxDuration((paceDurationMinutes * RAMPS + rampDurationMinutes * RAMPS) minutes)

}