package simulations

import io.gatling.core.Predef.{rampUsers, _}
import io.gatling.core.controller.inject.InjectionStep
import scala.concurrent.duration._

trait SimulationSettings extends Simulation {

  // ramp simulation settings
  val RAMPS = 2
  val maxUsers = 40
  val paceDurationMinutes = 3
  val rampDurationMinutes = 2

  def rampProfile(users: Int) : Iterable[InjectionStep] =
    List(
      rampUsers(users / RAMPS) over (rampDurationMinutes minutes),
      nothingFor(paceDurationMinutes minutes),
      rampUsers(users / RAMPS) over (rampDurationMinutes minutes),
      nothingFor(paceDurationMinutes minutes)
    )

  // constant simulation settings
  val requestsPerMinute = 250 // x4
  val durationInMinutes = 20

  def perMinute(rate: Double): Double = rate / 60

  def requestsPerMinuteProfile(rpm: Double) : Iterable[InjectionStep] =
    List(
      constantUsersPerSec(perMinute(rpm)) during(durationInMinutes minutes)
    )
}
