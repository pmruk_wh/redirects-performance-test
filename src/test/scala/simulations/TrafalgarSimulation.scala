package simulations

import io.gatling.core.Predef._
import scala.concurrent.duration._
import scenarios.PageViews._

class TrafalgarSimulation extends Simulation with SimulationSettings {

  setUp(viewTrafalgarPage.inject(rampProfile(maxUsers))).maxDuration((paceDurationMinutes * RAMPS + rampDurationMinutes * RAMPS) minutes)

}
