package simulations

import io.gatling.core.Predef._
import scenarios.PageViews._

import scala.concurrent.duration._

class OpenbetAndTrafalgarConstantSimulation extends Simulation with SimulationSettings {

  setUp(
    viewOpenbetPageConstatly.inject(requestsPerMinuteProfile(requestsPerMinute)),
    viewTrafalgarPageConstatly.inject(requestsPerMinuteProfile(requestsPerMinute)),
    viewTrafalgarPageConstatlyWithRedirects.inject(requestsPerMinuteProfile(requestsPerMinute)),
    viewOpenbetPageConstatlyWithRedirects.inject(requestsPerMinuteProfile(requestsPerMinute)),
  ).maxDuration(durationInMinutes minutes)

}
